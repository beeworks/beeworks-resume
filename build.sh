#!/usr/bin/env bash

OUTPUT_FILENAME=jurgen_lust
DEPLOY_BASE_TITLE="Jurgen Lust Resume"
DEPLOY_DESCRIPTION="Latest version"
OUTPUT_VERSION=0.0.1
ROOT_DIR=`pwd`
BUILD_DIR="${ROOT_DIR}/build"
OUTPUT_DIR="${ROOT_DIR}/output"
SRC_DIR="${ROOT_DIR}/src"
THEME_DIR="${ROOT_DIR}/theme"
LIB_DIR="${ROOT_DIR}/lib"
ENGLISH_INPUT_FILE=main

function clean {
	rm -rf "${BUILD_DIR}"
	rm -rf "${OUTPUT_DIR}"
}

function buildSourceFile {
	determineVersion
	local INPUT_FILE=$1
	local OUTPUT_SUFFIX=$2
	rm -rf "${BUILD_DIR}"
	mkdir -p "${BUILD_DIR}"
	cp -R "${SRC_DIR}"/* "${BUILD_DIR}"
    cp -R "${THEME_DIR}"/* "${BUILD_DIR}"
    cp -R "${LIB_DIR}"/* "${BUILD_DIR}"
	cd "${BUILD_DIR}"
	latexmk -pdflatex=lualatex -pdf "${INPUT_FILE}.tex"
	cd "${ROOT_DIR}"
	mkdir "${OUTPUT_DIR}"
	cp "$BUILD_DIR/${INPUT_FILE}.pdf" "${OUTPUT_DIR}/${OUTPUT_FILENAME}-${OUTPUT_SUFFIX}-${OUTPUT_VERSION}.pdf"
	rm -rf "${BUILD_DIR}"
}

function english {
    buildSourceFile ${ENGLISH_INPUT_FILE} "english"
}

function build {
    english
}

function determineVersion {
	LATEST_REVISION=`git rev-list --tags --skip=0 --max-count=1`
	CURRENT_BRANCH=`git rev-parse --abbrev-ref HEAD`

	if [ -z "${LATEST_REVISION}" ]
	then
		TAG_NAME="0.0.0"
	else
		TAG_NAME="`git describe --abbrev=0 --tags $LATEST_REVISION`"
    	DEPLOY_DESCRIPTION="`git tag -l --format='%(contents)' ${TAG_NAME}`"
	fi

	THIS_VERSION="${TAG_NAME%.*}.$((${TAG_NAME##*.}+1))"

	if [ $CURRENT_BRANCH == "master" ]
	then
	    THIS_VERSION=${TAG_NAME}
	else
    	THIS_VERSION="${TAG_NAME%.*}.$((${TAG_NAME##*.}+1))-SNAPSHOT"
	fi
	OUTPUT_VERSION="${THIS_VERSION}"
	echo "version [${OUTPUT_VERSION} ($CURRENT_BRANCH)]"
}

case "$1" in
        clean)
            clean
	    	;;
		version)
			determineVersion
			;;
		english)
		    english
		    ;;
        *)
            build
            ;;
esac
